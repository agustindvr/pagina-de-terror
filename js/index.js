    $(function(){
      $('[data-toggle="tooltip"]').tooltip();
      $('[data-toggle="popover"]').popover();
      $('.carousel').carousel({
        interval: 2000
      });

      $('#suscripcion').on('show.bs.modal', function(e){
        console.log('El modal esta funcionando')
        $('#suscripcionBtn').removeClass('btn-danger');
        $('#suscripcionBtn').addClass('btn-primary');
        $('#suscripcionBtn').prop('disabled', true);

      });
      $('#suscripcion').on('shown.bs.modal', function (e) {
        console.log('El modal se mostró')
      });
      $('#suscripcion').on('hide.bs.modal', function (e) {
        console.log('El modal se está ocultando')
      });
      $('#suscripcion').on('hidden.bs.modal', function (e) {
        console.log('El modal se ocultó')
        $('#suscripcionBtn').removeClass('btn-primary');
        $('#suscripcionBtn').addClass('btn-outline-danger');
        $('#suscripcionBtn').prop('disabled', false);
      });

      $('#contacto').on('show.bs.modal', function (e) {
        console.log('El modal esta funcionando')
        $('#contactoBtn').removeClass('btn-outline-success');
        $('#contactoBtn').addClass('btn-primary');
        $('#contactoBtn').prop('disabled', true);

      });
      $('#contacto').on('shown.bs.modal', function (e) {
        console.log('El modal se mostró')
      });
      $('#contacto').on('hide.bs.modal', function (e) {
        console.log('El modal se está ocultando')
      });
      $('#contacto').on('hidden.bs.modal', function (e) {
        console.log('El modal se ocultó')
        $('#contactoBtn').removeClass('btn-primary');
        $('#contactoBtn').addClass('btn-success');
        $('#contactoBtn').prop('disabled', false);
      });

    });

